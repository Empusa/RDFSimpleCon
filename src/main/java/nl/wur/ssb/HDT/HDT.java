package nl.wur.ssb.HDT;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.exceptions.ParserException;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdtjena.HDTGraph;

import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

public class HDT {

  public void save(Domain domain, File hdtfile) throws IOException, ParserException, Exception {

    // Resource to NTRIPLES
    File ntFile = NTRIPLES(domain);

    // conversion
    HDTSAVE(ntFile, hdtfile);
    // Cleaning temp file
    ntFile.delete();
  }

  public void merge(Domain domain, File HDT, File OUT) throws IOException, ParserException, Exception {
    // save resource to N-Triples
    File ntFile = NTRIPLES(domain);
    // save HDT to N-Triples
    hdt2rdf(HDT, ntFile);
    // Create HDT
    HDTSAVE(ntFile, OUT);
  }

  public void hdt2rdf(File hdtFile, File ntFile) throws IOException {
    // HDT2 RDF...
    org.rdfhdt.hdt.hdt.HDT hdt =
        org.rdfhdt.hdt.hdt.HDTManager.mapHDT(hdtFile.getAbsolutePath(), null);
    HDTGraph graph = new HDTGraph(hdt);
    Model model = ModelFactory.createModelForGraph(graph);
    OutputStream os;
    if (ntFile.exists()) {
      os = new FileOutputStream(ntFile, true);
    } else {
      os = new FileOutputStream(ntFile);
    }
    System.out.println("HDT2RDF: " + ntFile);
    model.write(os, "n-triples");
  }

  private File NTRIPLES(Domain domain) throws Exception {
    File tempFile = File.createTempFile("tmp", ".nt");
    // Cleaning after program is finished...
    tempFile.deleteOnExit();
    ///////
    RDFFormat format = RDFFormat.N_TRIPLE;
    
    domain.save(tempFile.getAbsolutePath(), format);
    System.out.println("NTRIPLES: " + tempFile);
    return tempFile;
  }

  private void HDTSAVE(File ntFile, File HDT) throws IOException, ParserException {
    String baseURI = "http://csb.wur.nl/genome/";
    String rdfInput = ntFile.getAbsolutePath();
    String inputType = "ntriples";
    org.rdfhdt.hdt.hdt.HDT hdt = org.rdfhdt.hdt.hdt.HDTManager.generateHDT(rdfInput, baseURI,
        RDFNotation.parse(inputType), new HDTSpecification(), null);
    System.out.println("HDTSAVE: " + HDT);
    hdt.saveToHDT(HDT.getAbsolutePath(), null);
  }

}