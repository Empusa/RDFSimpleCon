package nl.wur.ssb.RDFSimpleCon.api;

import java.util.Iterator;

import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

public class IndexedListIterable implements Iterable<Integer>
{
  private Domain domain;
  private Resource resource;
  private RDFNode object;
  public IndexedListIterable(Domain domain,Resource resource,RDFNode object)
  {
    this.domain = domain;
    this.resource = resource;
    this.object = object;
  }
  
  @Override
  public Iterator<Integer> iterator()
  {
    //This is not 
    return new IndexedListIterator(domain.con.listPattern(resource,null,object));
  }
}
