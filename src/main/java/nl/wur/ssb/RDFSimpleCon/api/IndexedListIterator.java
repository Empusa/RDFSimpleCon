package nl.wur.ssb.RDFSimpleCon.api;

import java.util.Iterator;

import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.StmtIterator;

public class IndexedListIterator implements Iterator<Integer>
{
  private StmtIterator it2;
  private Integer nextItem = null;
  IndexedListIterator(StmtIterator it2)
  {
    this.it2 = it2;
    this.prepareNext();
  }

  private void prepareNext()
  {
    this.nextItem = null;
    while(it2.hasNext())
    {
      String indexLit = it2.next().getPredicate().getURI();
      if(indexLit.startsWith(OWLThingImpl.indexPred))
      {
        try
        {
          int index = Integer.parseInt(indexLit.substring(OWLThingImpl.indexPredLength));
          if(index < 0)
            throw new RuntimeException("Index must positive integer");
          this.nextItem = index;
          break;
        }
        catch(NumberFormatException e)
        {
          throw new RuntimeException("Index must be integer");
        }
      }
    }
  }
 
  @Override
  public boolean hasNext()
  {
    return this.nextItem != null;
  }

  @Override
  public Integer next()
  {
    Integer toRet = this.nextItem;
    this.prepareNext();
    return toRet;
  }
}
