package nl.wur.ssb.RDFSimpleCon.api;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ListIndexException;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;

public class OWLThingImpl implements OWLThing
{
  static HashSet<String> preserved = new HashSet<String>();
  static HashMap<String,String> basePackageLookup = new HashMap<String,String>();
  static
  {
    for(String item : new String[]{"abstract","assert","boolean","break","byte","case","catch","char","class","const","continue","default","double","do","else","enum","extends","false","final","finally","float","for","goto","if","implements","import","instanceof","int","interface","long","native","new","null","package","private","protected","public","return","short","static","strictfp","super","switch","synchronized","this","throw","throws","transient","true","try","void","volatile","while"})
      preserved.add(item);
  }
  static final String indexPred = "http://www.w3.org/1999/02/22-rdf-syntax-ns#_";
  static final int indexPredLength = "http://www.w3.org/1999/02/22-rdf-syntax-ns#_".length();
  final Resource listFirst;
  final Resource listRest;
  final Resource listType;
  final Resource rdfType;
  final Resource rdfNil;
  Domain domain;
  Resource resource;
  boolean isDeleted = false;
   
  protected OWLThingImpl(Domain domain,Resource resource) 
  {
    this.domain = domain;
    this.resource = resource;
    
    listFirst = domain.con.createResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#first");
    listRest = domain.con.createResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#rest");
    listType = domain.con.createResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#List");
    rdfType = domain.con.createResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
    rdfNil = domain.con.createResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#nil");
    
    domain.setObject(resource,this);
    String typeIRI;
    try
    {
      typeIRI = (String)this.getClass().getField("TypeIRI").get(this);
    }
    catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e)
    {
      throw new RuntimeException("INTERNAL error",e);
    }
    this.domain.con.add(this.resource,"rdf:type",typeIRI);    
  }
   
  private void check()
  {
    if(this.isDeleted)
      throw new RuntimeException("Object is already deleted");
  }
  
  public void delete()
  {
    this.isDeleted = true;
    this.domain.con.removeObject(this.resource);
    this.domain.delObject(this.resource);
  }
  
  // String
  
  protected String getStringLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDstring))
        throw new RuntimeException("Expected string, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asLiteral().getString();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<String> getStringLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<String> toRet = new ArrayList<String>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDstring))
        throw new RuntimeException("Expected string, but found: " + object.getClass());
      toRet.add((String)object.asLiteral().getString());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addStringLit(String pred,String lit)
  {
    check();
    this.domain.con.addLit(this.resource,pred,lit);
  }
  
  protected void remStringLit(String pred,String lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.containsLit(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.remLit(this.resource,pred,lit);
  }
  
  protected void setStringLit(String pred,String lit) 
  {
    check();
    String oldVal = this.getStringLit(pred,true);
    if(oldVal != null)
      this.domain.con.remLit(this.resource,pred,oldVal);
    this.domain.con.addLit(this.resource,pred,lit);
  }
  
  //Integer
  
  protected Integer getIntegerLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDinteger))
        throw new RuntimeException("Expected integer, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asLiteral().getInt();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<Integer> getIntegerLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<Integer> toRet = new ArrayList<Integer>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDinteger))
        throw new RuntimeException("Expected integer, but found: " + object.getClass());
      toRet.add(object.asLiteral().getInt());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addIntegerLit(String pred,Integer lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remIntegerLit(String pred,int lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.containsLit(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setIntegerLit(String pred,Integer lit) 
  {
    check();
    Integer oldVal = this.getIntegerLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }
  
  //Double
  
  protected Double getDoubleLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDdouble))
        throw new RuntimeException("Expected double, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asLiteral().getDouble();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<Double> getDoubleLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<Double> toRet = new ArrayList<Double>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDdouble))
        throw new RuntimeException("Expected double, but found: " + object.getClass());
      toRet.add(object.asLiteral().getDouble());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addDoubleLit(String pred,Double lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remDoubleLit(String pred,int lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.containsLit(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setDoubleLit(String pred,Double lit) 
  {
    check();
    Double oldVal = this.getDoubleLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }

  //Float
  
  protected Float getFloatLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDfloat))
        throw new RuntimeException("Expected float, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asLiteral().getFloat();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<Float> getFloatLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<Float> toRet = new ArrayList<Float>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDfloat))
        throw new RuntimeException("Expected float, but found: " + object.getClass());
      toRet.add(object.asLiteral().getFloat());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addFloatLit(String pred,Float lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remFloatLit(String pred,int lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.containsLit(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setFloatLit(String pred,Float lit) 
  {
    check();
    Float oldVal = this.getFloatLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }
 //Long
  
  protected Long getLongLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !(object.asLiteral().getDatatype().equals(XSDDatatype.XSDinteger) || object.asLiteral().getDatatype().equals(XSDDatatype.XSDlong)))
        throw new RuntimeException("Expected long, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asLiteral().getLong();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<Long> getLongLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<Long> toRet = new ArrayList<Long>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !(object.asLiteral().getDatatype().equals(XSDDatatype.XSDinteger) || object.asLiteral().getDatatype().equals(XSDDatatype.XSDlong)))
        throw new RuntimeException("Expected long, but found: " + object.getClass());
      toRet.add(object.asLiteral().getLong());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addLongLit(String pred,Long lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remLongLit(String pred,long lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.containsLit(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setLongLit(String pred,Long lit) 
  {
    check();
    Long oldVal = this.getLongLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }
  
 //Boolean
  
  protected Boolean getBooleanLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDboolean))
        throw new RuntimeException("Expected boolean, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asLiteral().getBoolean();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<Boolean> getBooleanLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<Boolean> toRet = new ArrayList<Boolean>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDboolean))
        throw new RuntimeException("Expected boolean, but found: " + object.getClass());
      toRet.add(object.asLiteral().getBoolean());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addBooleanLit(String pred,Boolean lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remBooleanLit(String pred,int lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.containsLit(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setBooleanLit(String pred,Boolean lit) 
  {
    check();
    Boolean oldVal = this.getBooleanLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }
  
  //------------
  
  public String getClassTypeIri()
  {
    NodeIterator it = domain.con.getObjects(this.resource,"rdf:type");
    if(it.hasNext())
    {
      RDFNode toRet = it.next();
      if(it.hasNext())
        throw new RuntimeException("Multiple types on single instance are not allowed");
      if(!toRet.isResource())
        throw new RuntimeException("Type def should be resource");
      //TODO later we can further upgrade this
      return toRet.asResource().getURI();
    }
    return null;
  }
  
  private static String[] filter(String in[])
  {
    LinkedList<String> tmp2 = new LinkedList<String>();
    for(String item : in)
    {
      if(item.trim().equals(""))
        continue;
      if(item.trim().matches("[0-9]"))
        continue;
      if(preserved.contains(item))
        item = "_" + item;
      tmp2.add(item);
    }
    return tmp2.toArray(new String[0]);
  }
  
  public static String getBasePackage(String iri)
  {
    if(basePackageLookup.containsKey(iri))
      return basePackageLookup.get(iri);
    //TODO fix to accept all.resource stuff    
    iri = iri.replaceAll("/www\\.","/").replaceAll("/[0-9\\.]+","/").replaceAll("-","");
    while(iri.matches(".*//.*"))
      iri = iri.replaceAll("//","/");
    String toRet = iri.replaceAll("http:/(.*)[/#].*","$1");
    String tmpParts[] = toRet.split("/",2);
    String tmp[] = tmpParts[0].split("\\.");
    tmp = filter(tmp);
    ArrayUtils.reverse(tmp);
    toRet = StringUtils.join(tmp,".");
    if(tmpParts.length == 2)
    {
      tmp = tmpParts[1].split("/");
      tmp = filter(tmp);
      toRet = toRet + "." + StringUtils.join(tmp,".");
    }
    basePackageLookup.put(iri,toRet);
    return toRet;
  }
     
  protected <T> T getRef(String pred,boolean optional,Class expectedType) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return (T)domain.getObjectFromResource(object.asResource(),expectedType,true);
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected <T> List<T> getRefSet(String pred,boolean optional,Class expectedType) 
  {
    check();
    ArrayList<T> toRet = new ArrayList<T>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      toRet.add((T)domain.getObjectFromResource(object.asResource(),expectedType,true));
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addRef(String pred,OWLThing ref)
  {
    check();
    if(ref == null)
      throw new RuntimeException("Can not add null value");
    ref.validate();
    this.domain.con.add(this.resource,pred,ref.getResource());
  }
  
  protected void remRef(String pred,OWLThing ref,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.contains(this.resource,pred,ref.getResource()))
      throw new RuntimeException("Ref object not found");
    this.domain.con.rem(this.resource,pred,ref.getResource());
  }
  
  protected void setRef(String pred,OWLThing ref,Class expectedType)
  {
    check();
    if(ref != null && domain.check)
      ref.validate();
    OWLThingImpl oldVal = this.getRef(pred,true,expectedType);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal.resource);
    if(ref != null)
      this.domain.con.add(this.resource,pred,ref.getResource());
  }
  
  protected <T> T getRefListAtIndex(String pred,boolean isOptional,Class expectedType,int index) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,OWLThingImpl.indexPred + index);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Index can only used once");
      it = domain.con.getObjects(this.resource,pred);
      if(!it.hasNext())
        throw new RuntimeException("Item must also be referenced by predicate");
      return (T)domain.getObjectFromResource(object.asResource(),expectedType,true);
    }
    it = domain.con.getObjects(this.resource,pred);
    if(!isOptional && !it.hasNext())
      throw new RuntimeException("Cardinality =0, expected minimum cardinality 1");
    return null;
  }
  
  protected <T> List<T> getRefList(String pred,boolean optional,Class expectedType) 
  {
    check();
    ArrayList<T> toRet = new ArrayList<T>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      for(Integer index : new IndexedListIterable(domain,this.resource,object))
      {
        toRet.ensureCapacity(index + 1);
        while(toRet.size() <= index)
          toRet.add(null);
        toRet.set(index,(T)domain.getObjectFromResource(object.asResource(),expectedType,true));
      }
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addRefList(String pred,OWLThing ref)
  {
    check();
    if(ref == null)
      throw new RuntimeException("can not add null value");
    ref.validate();
    //First get the size of the array
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    int maxIndex = -1;
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      for(Integer index : new IndexedListIterable(domain,this.resource,object))
      {
        maxIndex = Integer.max(index,maxIndex);
      }
    }
    this.domain.con.add(this.resource,pred,ref.getResource());
    this.domain.con.add(this.resource,OWLThingImpl.indexPred + (maxIndex + 1),ref.getResource());
  }
  
  /*
   * Set value at index, if ref == null then remove item at given index
   */
  protected void setRefList(String pred,OWLThing ref,boolean isOptional,int index)
  {
    check();
    if(ref != null)
    {
      ref.validate();
      this.domain.con.add(this.resource,pred,ref.getResource());
      this.domain.con.add(this.resource,OWLThingImpl.indexPred + index,ref.getResource());
    }
    else
    {
      this.remRefListAtIndex(pred,isOptional,index);
    }

  }
  
  /*
   * Remove item from list
   * 
   * check if index has only 1 object 
   * 
   * if item not any more link to list remove pred also
   */
  private void remRefListAtIndex(String pred,boolean isOptional,int index)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,OWLThingImpl.indexPred + index);
    
    RDFNode node = null;
    if(it.hasNext())
      node = it.next();
    else
      return;
    if(it.hasNext())
      throw new RuntimeException("Two items at index: " + index);
    
    this.domain.con.rem(this.resource,OWLThingImpl.indexPred + index,node);
    if(!(new IndexedListIterable(domain,this.resource,node)).iterator().hasNext()) 
    {
      this.domain.con.rem(this.resource,pred,node);
    }
    
    it = domain.con.getObjects(this.resource,pred);
    if(!isOptional && !it.hasNext())
      throw new RuntimeException("Cardinality should be >=1");
  }
 
  
  public void remRefList(String pred,OWLThing item,boolean isOptional)
  {
    check();
    RDFNode node = item.getResource();
    LinkedList<Integer> toDel = new LinkedList<Integer>();
    for(Integer index : new IndexedListIterable(domain,this.resource,node))
      toDel.add(index);
    for(int index : toDel)
    {
      this.domain.con.rem(this.resource,OWLThingImpl.indexPred + index,node);
    }
    this.domain.con.rem(this.resource,pred,node);
    
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(!isOptional && !it.hasNext())
      throw new RuntimeException("Cardinality should be >=1");
  }
  
  //----List list operations
  protected <T> T getRefListAtIndexList(String pred,boolean isOptional,Class expectedType,int index) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(!isOptional && !it.hasNext())
      throw new RuntimeException("Cardinality =0, expected minimum cardinality 1");
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.canAs(RDFList.class))
        throw new RuntimeException("Expect list object to be list");
      RDFList theList = object.as(RDFList.class);
      if(!theList.isValid())
        throw new RuntimeException("List is not valid");
      RDFNode item = theList.get(index);
      if(!item.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      if(item == rdfNil)
        return null;
      else
        return (T)domain.getObjectFromResource(item.asResource(),expectedType,true);
    }
    throw new ListIndexException("Lists index out of bound: " + index);
  }
  
  protected <T> List<T> getRefListList(String pred,boolean optional,Class expectedType) 
  {
    ArrayList<T> toRet = new ArrayList<T>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(!optional && !it.hasNext())
      throw new RuntimeException("Cardinality =0, expected minimum cardinality 1");
    
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.canAs(RDFList.class))
        throw new RuntimeException("Expect list object to be list");
      RDFList theList = object.as(RDFList.class);
      if(!theList.isValid())
        throw new RuntimeException("List is not valid");
      ExtendedIterator<RDFNode> iterator = theList.iterator();
      while(iterator.hasNext())
      {
        RDFNode item = iterator.next();
        if(!item.isResource())
          throw new RuntimeException("Expected resource, but found: " + object);
        if(item == rdfNil)
          toRet.add(null);
        else
          toRet.add((T)domain.getObjectFromResource(item.asResource(),expectedType,true));
      }
    }
    return toRet;
  }
  
  protected void addRefListList(String pred,OWLThing ref)
  {
    check();   
    Resource toAdd = rdfNil;
    if(ref != null)
    {
      ref.validate();
      toAdd = ref.getResource();
    } 
    NodeIterator it = domain.con.getObjects(this.resource,pred);

    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.canAs(RDFList.class))
        throw new RuntimeException("Expect list object to be list");
      RDFList list = object.as(RDFList.class);
      if(!list.isValid())
        throw new RuntimeException("List is not valid");
      list.add(toAdd);
    }
    else
    {
      RDFList list = domain.con.getModel().createList();
      list = list.with(toAdd);
      this.domain.con.add(this.resource,pred,list);
    }
  }
  
  /*
   * Set value at index
   */
  protected void setRefListList(String pred,OWLThing ref,boolean isOptional,int index)
  {
    check();
    Resource toSet = rdfNil;
    if(ref != null)
    {
      ref.validate();
      toSet = ref.getResource();
    } 
    NodeIterator it = domain.con.getObjects(this.resource,pred);
     
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.canAs(RDFList.class))
        throw new RuntimeException("Expect list object to be list");
      RDFList list = object.as(RDFList.class);
      if(!list.isValid())
        throw new RuntimeException("List is not valid");
      list.replace(index,toSet);
    }
    else
    {
      throw new ListIndexException("Lists index out of bound: " + index);
    }
  }
  
  /*
   * Remove item from list
   * 
   * check if index has only 1 object 
   * 
   * if item not any more link to list remove pred also
   
  private void remRefListAtIndexList(String pred,boolean isOptional,int index)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
     
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.canAs(RDFList.class))
        throw new RuntimeException("Expect list object to be list");
      RDFList list = object.as(RDFList.class);
      if(!list.isValid())
        throw new RuntimeException("List is not valid");
      RDFList newList = list.remove(list.get(index));
      if(index == 0)
      {
        this.domain.con.rem(this.resource,pred,list.getURI());
        this.domain.con.add(this.resource,pred,newList.getURI());
      }
    }
    else
    {
      throw new ListIndexException("Lists index out of bound: " + index);
    }
  }*/
 
  
  public void remRefListList(String pred,OWLThing item,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.canAs(RDFList.class))
        throw new RuntimeException("Expect list object to be list");
      RDFList list = object.as(RDFList.class);
      if(!list.isValid())
        throw new RuntimeException("List is not valid");
      RDFList newList = list.remove(item.getResource());
      if(list != newList)
      {
        //TODO bug report to JENA
        if(newList.isAnon() || !newList.getURI().equals(rdfNil.getURI()))
          this.domain.con.add(this.resource,pred,newList);
        else if(!isOptional)
          throw new RuntimeException("Cardinality should be >=1");
  
        this.domain.con.rem(this.resource,pred,list);
      }
    }
  }
   
  
  //Enum type
  
  private EnumClass getEnumFromIri(String iri,Class expectedType)
  {
    EnumClass toRet = null;
    try
    {
      toRet = (EnumClass)expectedType.getMethod("make",String.class).invoke(null,iri);
    }
    catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
    {
      throw new RuntimeException("Interal Error",e);
    }     
    return toRet;
  }
  
  protected <T> T getEnum(String pred,boolean optional,Class expectedType) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return (T)this.getEnumFromIri(object.asResource().getURI(),expectedType);
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected <T> List<T> getEnumSet(String pred,boolean optional,Class expectedType) 
  {
    check();
    ArrayList<T> toRet = new ArrayList<T>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      toRet.add((T)this.getEnumFromIri(object.asResource().getURI(),expectedType));
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addEnum(String pred,EnumClass ref)
  {
    check();
    this.domain.con.add(this.resource,pred,ref.getIRI());
  }
  
  protected void remEnum(String pred,EnumClass ref,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.contains(this.resource,pred,ref.getIRI()))
      throw new RuntimeException("Ref object not found");
    this.domain.con.rem(this.resource,pred,ref.getIRI());
  }
  
  protected void setEnum(String pred,EnumClass ref,Class expectedType)
  {
    check();
    EnumClass oldVal = this.getEnum(pred,true,expectedType);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal.getIRI());
    this.domain.con.add(this.resource,pred,ref.getIRI());
  }  
  
  // Externalref
  
  protected String getExternalRef(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return object.asResource().getURI();
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<String> getExternalRefSet(String pred,boolean optional)
  {
    check();
    ArrayList<String> toRet = new ArrayList<String>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isResource())
        throw new RuntimeException("Expected resource, but found: " + object.getClass());
      toRet.add((String)object.asResource().getURI());
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addExternalRef(String pred,String lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remExternalRef(String pred,String lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.contains(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setExternalRef(String pred,String lit) 
  {
    check();
    String oldVal = this.getExternalRef(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);    
  }
  
  //LocalDate
  
  protected LocalDate getDateLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDdate))
        throw new RuntimeException("Expected date, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return LocalDate.parse(object.asLiteral().getValue().toString());
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<LocalDate> getDateLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<LocalDate> toRet = new ArrayList<LocalDate>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDdate))
        throw new RuntimeException("Expected date, but found: " + object.getClass());
      toRet.add(LocalDate.parse(object.asLiteral().getValue().toString()));
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addDateLit(String pred,LocalDate lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remDateLit(String pred,LocalDate lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.contains(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setDateLit(String pred,LocalDate lit) 
  {
    check();
    LocalDate oldVal = this.getDateLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }
  
  //DateTime literal
 //LocalDate
  
  protected LocalDateTime getDateTimeLit(String pred,boolean optional) 
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    if(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDdateTime))
        throw new RuntimeException("Expected dateTime, but found: " + object);
      if(it.hasNext())
        throw new RuntimeException("Cardinality >1, expected cardinality " + (optional ? "0 or 1" : "1"));
      return LocalDateTime.parse(object.asLiteral().getValue().toString());
    }
    if(!optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1");
    return null;
  }
  
  protected List<LocalDateTime> getDateTimeLitSet(String pred,boolean optional)
  {
    check();
    ArrayList<LocalDateTime> toRet = new ArrayList<LocalDateTime>();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    while(it.hasNext())
    {
      RDFNode object = it.next();
      if(!object.isLiteral() || !object.asLiteral().getDatatype().equals(XSDDatatype.XSDdateTime))
        throw new RuntimeException("Expected dateTime, but found: " + object.getClass());
      toRet.add(LocalDateTime.parse(object.asLiteral().getValue().toString()));
    }
    if(toRet.size() == 0 && !optional)
      throw new RuntimeException("Cardinality =0, expected cardinality 1..N");
    return toRet;
  }
  
  protected void addDateTimeLit(String pred,LocalDateTime lit)
  {
    check();
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void remDateTimeLit(String pred,LocalDateTime lit,boolean isOptional)
  {
    check();
    NodeIterator it = domain.con.getObjects(this.resource,pred);
    
    if(!isOptional && (it.hasNext() == false || it.next() != null && it.hasNext() == false))
      throw new RuntimeException("Cardinality should be >=1");
    
    if(!domain.con.contains(this.resource,pred,lit))
      throw new RuntimeException("String literal not found");
    this.domain.con.rem(this.resource,pred,lit);
  }
  
  protected void setDateTimeLit(String pred,LocalDateTime lit) 
  {
    check();
    LocalDateTime oldVal = this.getDateTimeLit(pred,true);
    if(oldVal != null)
      this.domain.con.rem(this.resource,pred,oldVal);
    this.domain.con.add(this.resource,pred,lit);
  }
  
  protected void checkCardMin1(String pred)
  {
    if(!this.domain.con.getObjects(this.resource,pred).hasNext())
      throw new RuntimeException("Cardinality should be >=1 for property: " + pred);
  }
  
  public void validate()
  {
    
  }
  public Resource getResource()
  {
    return this.resource;
  }
  
  public String getUniqId()
  {
    return this.resource.toString();
  }
}
