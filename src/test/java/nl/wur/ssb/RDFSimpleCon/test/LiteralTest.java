package nl.wur.ssb.RDFSimpleCon.test;

import static org.junit.Assert.assertEquals;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

import org.apache.jena.query.ResultSet;
import org.junit.Test;

public class LiteralTest
{
  @Test
  public void testString() throws Exception {  
  	RDFSimpleCon con = new RDFSimpleCon("");
  	con.addLit("http://test.com/testSubject","http://test.com/testPred","teststring");
  	ResultSet result = con.runQueryDirect("SELECT * WHERE { ?x ?y \"teststring\"}"); //^^<http://www.w3.org/2001/XMLSchema#string>}
    assertEquals(true,result.hasNext());
  //	result = con.runQueryDirect("SELECT * WHERE { ?x ?y \"teststring\"^^<http://www.w3.org/2001/XMLSchema#string>}"); //^^<http://www.w3.org/2001/XMLSchema#string>}
  //  assertEquals(true,result.hasNext());
  }
}
