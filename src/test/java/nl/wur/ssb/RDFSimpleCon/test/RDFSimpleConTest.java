package nl.wur.ssb.RDFSimpleCon.test;

import junit.framework.TestCase;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.impl.ModelCom;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.File;
import java.util.Scanner;

public class RDFSimpleConTest extends TestCase {

    public void testRDFSimpleCon() throws Exception {
        RDFSimpleCon rdfSimpleCon = new RDFSimpleCon("https://sparql.uniprot.org/sparql");
        rdfSimpleCon.runQuery("test.sparql", true);

//        HDTCat hdtCat = new HDTCat();
        System.err.println("done");
    }

    public void testRDFSimpleConLoad() throws Exception {
        Domain domain = new Domain("file://./src/test/resources/test.ttl");
        domain.getRDFSimpleCon().setNsPrefix("test", "http://test.com");
        domain.save("test.ttl");

        Scanner scanner = new Scanner(new File("test.ttl"));
        while (scanner.hasNextLine())
            System.out.println(scanner.nextLine());
    }

    public void testRDFSimpleConHDT() throws Exception {
        String baseURI = "http://example.com/mydataset";
        String rdfInput = "./src/test/resources/test.ttl";
        String inputType = "turtle";
        String hdtOutput = "./src/test/resources/test.hdt";

        // Create HDT from RDF file
        HDT hdt = HDTManager.generateHDT(
                rdfInput,         // Input RDF File
                baseURI,          // Base URI
                RDFNotation.parse(inputType), // Input Type
                new HDTSpecification(),   // HDT Options
                null              // Progress Listener
        );

        // Save generated HDT to a file
        hdt.saveToHDT(hdtOutput, null);

        hdt = HDTManager.loadIndexedHDT("./src/test/resources/test.hdt", null);
        HDTGraph graph = new HDTGraph(hdt);
        Model model = new ModelCom(graph);
        model.listStatements().forEachRemaining(statement -> {
            System.err.println(statement);
        });
    }
}
