#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest of RDFSimpleCon
git -C "$DIR" pull

# Obtain and build HDT-JAVA
# git clone https://gitlab.com/wurssb/hdt-java $DIR/../hdt-java
# $DIR/../hdt-java/install.sh

gradle build -b "$DIR/build.gradle" 

cp $DIR/build/libs/*jar $DIR/

mvn install:install-file -Dfile=$DIR/build/libs/RDFSimpleCon-0.1.jar -DgroupId=nl.wur.ssb.RDFSimpleCon -DartifactId=RDFSimpleCon -Dversion=0.1 -Dpackaging=jar
